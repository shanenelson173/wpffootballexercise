﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfFootballExercise.ViewModel;

namespace WpfFootballExercise.wpfForms
{
    /// <summary>
    /// Interaction logic for wpfFootBallExercise.xaml
    /// </summary>
    public partial class wpfFootBallExercise : Window
    {
        public wpfFootBallExercise()
        {
            InitializeComponent();
            this.DataContext = new FootballExerciseViewModel();
        }
    }
}
