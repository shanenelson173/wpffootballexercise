﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfFootballExercise.Model
{
    /// <summary>
    /// Model for wpfFootBallExercise
    /// </summary>
    public class FootBallModel
    {
        #region Properties

        public string TeamName { get; set; }
        public int GoalsScoredFor { get; set; }
        public int GoalsScoredAgainst { get; set; }
        public int DifferenceGoalsScoredForAgainst { get; set; }

        #endregion 

    }
}
