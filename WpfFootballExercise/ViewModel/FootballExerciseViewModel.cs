﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfFootballExercise.DAL;
using WpfFootballExercise.Model;

namespace WpfFootballExercise.ViewModel
{
    /// <summary>
    /// Interaction logic for wpfFootBallExercise.xaml
    /// </summary>
    public class FootballExerciseViewModel
    {
        #region Properties
        
        public string TeamNameProperty { get; set; }
        public int GoalScoredForProperty { get; set; }
        public int GoalScoredAgainstProperty { get; set; }
        public int DifferenceProperty { get; set; }
        public bool IsOpenProperty { get; set; }
        public string ErrorMessageProperty { get; set; }
        
        #endregion 

        #region Constructor
        
        public FootballExerciseViewModel()
        {
            Repository repository = new Repository(); // Create an instance of the Repository.

            // Initilize error message variables.
            string ErrorMessage = "";
            IsOpenProperty = false;

            // Now load the list of FootBallModels.
            List<FootBallModel> FootBallRows = repository.Select(ref ErrorMessage);

            if (FootBallRows != null)
            {
                FootBallModel ReturnArgs = CalculateTeamWithSmallestDifference(FootBallRows);
                if (ReturnArgs != null)
                {
                    this.TeamNameProperty = ReturnArgs.TeamName;
                    this.GoalScoredForProperty = ReturnArgs.GoalsScoredFor;
                    this.GoalScoredAgainstProperty = ReturnArgs.GoalsScoredAgainst;
                    this.DifferenceProperty = ReturnArgs.DifferenceGoalsScoredForAgainst;
                }
            }

            // If there are any error the code below, displays the message to the user here.
            if (ErrorMessage != "")
            {
                IsOpenProperty = true;
                ErrorMessageProperty = "Error Message : " + ErrorMessage;
            }
        }

        #endregion

        #region Business Logic

        /// <summary>
        /// Method used to calculate the team with the smallest difference between goals scored for and against.
        /// </summary>
        /// <param name="FootBallRows"></param>
        /// <returns></returns>
        private FootBallModel CalculateTeamWithSmallestDifference(List<FootBallModel> FootBallRows)
        {     
            /*
             * Based on the assumption that the smallest difference is a whole number. 
             * This method converts real numbers to whole numbers and sorts in ascending order.
             * once sorted, the result will be in the first element of the list.
            */

            if (FootBallRows == null)
                return null;

            int i = 0;
            int getIndex = 0;

            //Linq expression calculate difference and sort ascending order.
            var ResultWithDifference = from s in FootBallRows
                                       select new {
                                       Difference = (s.GoalsScoredFor - s.GoalsScoredAgainst) < 0 ? 
                                       ((s.GoalsScoredFor - s.GoalsScoredAgainst) * -1) : 
                                       (s.GoalsScoredFor - s.GoalsScoredAgainst), getIndex = i++};

            
            var Query = ResultWithDifference.OrderBy(n => n.Difference);
            getIndex = Query.First().getIndex;
            FootBallRows[getIndex].DifferenceGoalsScoredForAgainst = Query.First().Difference;

            return FootBallRows[getIndex];
        }

        #endregion

    }
}
