﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfFootballExercise.Model;
using System.IO;
using System.Configuration;


namespace WpfFootballExercise.DAL
{
    /// <summary>
    /// Repository for wpfFootBallExercise
    /// </summary>
    public class Repository
    {      
        /// <summary>
        /// Performs a select on the football.dat table.
        /// </summary>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public List<FootBallModel> Select(ref string ErrorMessage)
        {
            List<FootBallModel> FootBallRows = new List<FootBallModel>();
            string AppPath = ConfigurationManager.AppSettings["AppPath"] != null ? ConfigurationManager.AppSettings["AppPath"].ToString():"";
            string DatabasePath = ConfigurationManager.AppSettings["DatabasePath"] != null ? ConfigurationManager.AppSettings["DatabasePath"].ToString(): "";

            try
            {
                //Iterate thru the .Dat file, in an effort to extract all qualified rows into
                //the FootBallModel.
                using (StreamReader streamReader = new StreamReader(AppPath + DatabasePath))
                {
                    string Row;  
                    while ((Row = streamReader.ReadLine()) != null)
                    {
                        FootBallModel Record = ColumnExtractionRoutine(Row);
                        if (Record != null)
                            FootBallRows.Add(Record);
                    }
                }

                return FootBallRows;
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Method to extract column data and filter out unwanted rows.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private FootBallModel ColumnExtractionRoutine(string row)
        {
            /* Working on the assumption that the football.dat file column position are fixed.
             * Hence the position are hard coded.
             */
            FootBallModel Record = new FootBallModel();        

            if (row=="" || Record==null)
                return null;

                
            Record.TeamName = row.Substring(1, 22).Trim();

            //Applies the condition below to omit header and badly formed rows.
            if (Record.TeamName != "Team" && Record.TeamName != "--------------------")
            {
                Record.GoalsScoredFor = row.Substring(43, 4).Trim() == "" ? 0 : int.Parse(row.Substring(43, 4).Trim());
                Record.GoalsScoredAgainst = row.Substring(50, 6).Trim() == "" ? 0 : int.Parse(row.Substring(50, 6).Trim());
                Record.DifferenceGoalsScoredForAgainst = 0;
            }
            else
            {
                Record = null;
            }

            return Record;
        }
    }
}
