﻿using WpfFootballExercise.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace WpfFootballExercise_TestProject
{
    
    
    /// <summary>
    ///This is a test class for FootBallModelTest and is intended
    ///to contain all FootBallModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FootBallModelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for FootBallModel Constructor
        ///</summary>
        [TestMethod()]
        public void FootBallModelConstructorTest()
        {
            FootBallModel target = new FootBallModel();
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for DifferenceGoalsScoredForAgainst
        ///</summary>
        [TestMethod()]
        public void DifferenceGoalsScoredForAgainstTest()
        {
            FootBallModel target = new FootBallModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.DifferenceGoalsScoredForAgainst = expected;
            actual = target.DifferenceGoalsScoredForAgainst;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GoalsScoredAgainst
        ///</summary>
        [TestMethod()]
        public void GoalsScoredAgainstTest()
        {
            FootBallModel target = new FootBallModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.GoalsScoredAgainst = expected;
            actual = target.GoalsScoredAgainst;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GoalsScoredFor
        ///</summary>
        [TestMethod()]
        public void GoalsScoredForTest()
        {
            FootBallModel target = new FootBallModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.GoalsScoredFor = expected;
            actual = target.GoalsScoredFor;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TeamName
        ///</summary>
        [TestMethod()]
        public void TeamNameTest()
        {
            FootBallModel target = new FootBallModel(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.TeamName = expected;
            actual = target.TeamName;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
