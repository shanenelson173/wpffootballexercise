﻿using WpfFootballExercise.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WpfFootballExercise.Model;
using System.Collections.Generic;

namespace WpfFootballExercise_TestProject
{
    
    
    /// <summary>
    ///This is a test class for RepositoryTest and is intended
    ///to contain all RepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Repository Constructor
        ///</summary>
        [TestMethod()]
        public void RepositoryConstructorTest()
        {
            Repository target = new Repository();
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ColumnExtractionRoutine
        ///</summary>
        [TestMethod()]
        [DeploymentItem("WpfFootballExercise.exe")]
        public void ColumnExtractionRoutineTest()
        {
            Repository_Accessor target = new Repository_Accessor(); // TODO: Initialize to an appropriate value
            string row = string.Empty; // TODO: Initialize to an appropriate value
            FootBallModel expected = null; // TODO: Initialize to an appropriate value
            FootBallModel actual;
            actual = target.ColumnExtractionRoutine(row);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Select
        ///</summary>
        [TestMethod()]
        public void SelectTest()
        {
            Repository target = new Repository(); // TODO: Initialize to an appropriate value
            string ErrorMessage = string.Empty; // TODO: Initialize to an appropriate value
            string ErrorMessageExpected = "Empty path name is not legal."; // TODO: Initialize to an appropriate value
            List<FootBallModel> expected = null; // TODO: Initialize to an appropriate value
            List<FootBallModel> actual;
            actual = target.Select(ref ErrorMessage);
            Assert.AreEqual(ErrorMessageExpected, ErrorMessage);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
