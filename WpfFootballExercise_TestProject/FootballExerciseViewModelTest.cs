﻿using WpfFootballExercise.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WpfFootballExercise.Model;
using System.Collections.Generic;

namespace WpfFootballExercise_TestProject
{
    
    
    /// <summary>
    ///This is a test class for FootballExerciseViewModelTest and is intended
    ///to contain all FootballExerciseViewModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FootballExerciseViewModelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for FootballExerciseViewModel Constructor
        ///</summary>
        [TestMethod()]
        public void FootballExerciseViewModelConstructorTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel();
            //Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CalculateTeamWithSmallestDifference
        ///</summary>
        [TestMethod()]
        [DeploymentItem("WpfFootballExercise.exe")]
        public void CalculateTeamWithSmallestDifferenceTest()
        {
            FootballExerciseViewModel_Accessor target = new FootballExerciseViewModel_Accessor(); // TODO: Initialize to an appropriate value
            List<FootBallModel> FootBallRows = null; // TODO: Initialize to an appropriate value
            FootBallModel expected = null; // TODO: Initialize to an appropriate value
            FootBallModel actual;
            actual = target.CalculateTeamWithSmallestDifference(FootBallRows);
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DifferenceProperty
        ///</summary>
        [TestMethod()]
        public void DifferencePropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.DifferenceProperty = expected;
            actual = target.DifferenceProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ErrorMessageProperty
        ///</summary>
        [TestMethod()]
        public void ErrorMessagePropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.ErrorMessageProperty = expected;
            actual = target.ErrorMessageProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GoalScoredAgainstProperty
        ///</summary>
        [TestMethod()]
        public void GoalScoredAgainstPropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.GoalScoredAgainstProperty = expected;
            actual = target.GoalScoredAgainstProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GoalScoredForProperty
        ///</summary>
        [TestMethod()]
        public void GoalScoredForPropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.GoalScoredForProperty = expected;
            actual = target.GoalScoredForProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IsOpenProperty
        ///</summary>
        [TestMethod()]
        public void IsOpenPropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.IsOpenProperty = expected;
            actual = target.IsOpenProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TeamNameProperty
        ///</summary>
        [TestMethod()]
        public void TeamNamePropertyTest()
        {
            FootballExerciseViewModel target = new FootballExerciseViewModel(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.TeamNameProperty = expected;
            actual = target.TeamNameProperty;
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
