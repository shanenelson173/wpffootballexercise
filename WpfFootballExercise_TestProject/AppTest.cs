﻿using WpfFootballExercise;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace WpfFootballExercise_TestProject
{
    
    
    /// <summary>
    ///This is a test class for AppTest and is intended
    ///to contain all AppTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AppTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for App Constructor
        ///</summary>
        [TestMethod()]
        public void AppConstructorTest()
        {
            int expected = 0;
            int actual = 0;
            try
            {
            App target = new App();
            Assert.AreEqual(expected, actual);
            }
            catch
            {               
                Assert.AreEqual(expected, actual);                
            }            
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        public void InitializeComponentTest()
        {
            int expected = 0;
            int actual = 0;
            try
            {
                App target = new App(); // TODO: Initialize to an appropriate value
                target.InitializeComponent();
                Assert.AreEqual(expected, actual);
            }
            catch
            {  
                Assert.AreEqual(expected, actual);
            }
            
        }

        /// <summary>
        ///A test for Main
        ///</summary>
        [TestMethod()]
        public void MainTest()
        {
            int expected = 0;
            int actual = 0;
            try
            {
                App.Main();
                Assert.AreEqual(expected, actual);
            }
            catch
            {                
                Assert.AreEqual(expected, actual);
            }
        }
    }
}
